Astuce Linux
============

crontab
-------

Edition de la cron:

    crontab -e

Pour exécuter un script Python toutes les minutes:

    * * * * * python /home/pi/data/mbus_reader.py -t /home/pi/data/sharky.xml >> /dev/null 2>&1

Mettre à jour la date sans NTP
------------------------------

### CURL ###
    sudo date -u --set="$(curl -H 'Cache-Control: no-cache' -sD - http://google.com |grep '^Date:' |cut -d' ' -f3-6)"

### WGET ###
    sudo date -s "$(wget -qSO- --max-redirect=0 google.com 2>&1 | grep Date: | cut -d' ' -f5-8)Z"