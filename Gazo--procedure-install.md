Procédure d'installation du gazo
================================
Installation SD
---------------

[https://wiki.debian.org/InstallingDebianOn/Allwinner#Installing_from_an_SD_card_image](https://wiki.debian.org/InstallingDebianOn/Allwinner#Installing_from_an_SD_card_image) et suivre les instructions

Installation config-perso
-------------------------

    $ su
    # apt-get install sudo git
    # usermod -aG sudo <user>
    $ git clone https://github.com/oryxr/config-perso
    $ cd config-perso
    $ ./install_config.sh
    $ chsh  ->  /bin/zsh
    $ su
    # chsh  -> /bin/zsh
    
    $ sudo dpkg-reconfigure locales
    $ sudo dpkg-reconfigure tzdata

Installation i2c
----------------

    apt-get update
    apt-get install i2c-tools
    modprobe -a i2c-dev

ajout du module au démarrage mettre `i2c-dev` à la fin du fichier `/etc/modules`

Controler la bonne presence du materiel i2c
-------------------------------------------

    i2cdetect -y 2

Rendre le nom persistant du convertisseur USB-RS485
---------------------------------------------------

Brancher le convertisseur, regarder dans quel `/dev/tty...` il est mis puis :

    udevadm info -a -n /dev/ttyUSB1

Créer un fichier `/etc/udev/rules.d/99-usb-serial.rules` avec :

    SUBSYSTEM=="tty", ATTRS{idVendor}=="", ATTRS{idProduct}=="", ATTRS{serial}=="", SYMLINK+="ttyRS485"

Link : http://hintshop.ludvig.co.nz/show/persistent-names-usb-serial-devices/

Compléter les éléments avec les équivalents trouvé précédement.

Installation de Python 3.5
--------------------------

### Installation

    sudo apt-get install build-essential libncurses5-dev libncursesw5-dev libreadline6-dev libdb-dev libgdbm-dev libsqlite3-dev libssl-dev libbz2-dev libexpat1-dev liblzma-dev zlib1g-dev gcc make
    wget https://www.python.org/ftp/python/3.5.1/Python-3.5.1.tar.xz
    tar -Jxvf Python-3.5.1.tar.xz
    cd Python-3.5.1
    ./configure --prefix=/opt/python-3.5.1
    make
    sudo make install

### Virtualenv

    /opt/python-3.5.1/bin/pyvenv ~/.virtualenv/python-3.5.1
    source ~/.virtualenv/python-3.5.1/bin/activate

### Installation package

    pip install paho-mqtt pyA20 stackFifoLifo
    git clone https://github.com/bashwork/pymodbus.git -b python3
    cd pymodbus

Supprimer la dépendance de Twisted dans `setup.py` puis install pymodbus

    python setup.py install

### Petites vérification pour que ça fonctionne
Vérifier que dans `/etc/hosts` il y ait bien `127.0.0.1 localhost`
et rajouter dans `/etc/resolv.conf` (pas sûr de l'utilité)

    nameserver 8.8.8.8
    nameserver 8.8.4.4

Installation de Crossbar
------------------------

    sudo apt-get install libffi-dev

Installation MQTT
-----------------
S'assurer d'avoir installé la dernière version de Mosquitto avec le websocket activé et ajouter dans `/etc/mosquitto/mosquitto.conf`

    port 1883
    listener 1884
    protocol websockets

source : http://mosquitto.org/2013/01/mosquitto-debian-repository/

Installation Emoncms
--------------------
Emoncms : https://github.com/emoncms/emoncms/tree/master/docs/RaspberryPi

