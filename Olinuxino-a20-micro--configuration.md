Installation des olinuxino
==========================

Installation du système
-----------------------
- Réalisation de la procédure indiqué sur [wiki.debian.org](https://wiki.debian.org/InstallingDebianOn/Allwinner#Installing_from_an_SD_card_image)
- Installation des toolchains pour compilation du noyau: https://linux-sunxi.org/Toolchain
- Crosscompilation du Mainline Kernel: https://linux-sunxi.org/Mainline_Kernel_Howto#Kernel_source
- Création des composants de boot : https://linux-sunxi.org/Mainline_Kernel_Howto#Boot
- Copie de `boot.cmd` dans `/boot` et création de `boot.scr` :


    mkimage -A arm -T script -C none -n "My Boot.scr" -d boot.cmd boot.scr

- Copie du Kernel dans `/boot`: https://linux-sunxi.org/Manual_build_howto#Install_Bootloader_to_the_SD-Card

- Installation i2c-tools (puis vérifier si i2c est bien activé.

#### Sources :

[linux-sunxi: A20-micro](https://linux-sunxi.org/A20-olinuxino-micro)

[wiki.debian.org](https://wiki.debian.org/InstallingDebianOn/Allwinner#Installing_from_an_SD_card_image)

[LiveSuit](http://linux-sunxi.org/LiveSuit)

[Mainline Debian HowTo](http://linux-sunxi.org/Mainline_Debian_HowTo#Using_a_ready-made_installation_SD_card_image_instead_of_bootstrapping)

[sunxi-bootsetup](https://github.com/ssvb/sunxi-bootsetup/releases)