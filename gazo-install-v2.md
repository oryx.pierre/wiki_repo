Install gazo v2
===============

Install SD
----------
Ecrire SD avec Win32Disk
Activer l'auto connexion réseau
Installer ntpdate:

    sudo apt-get install ntpdate
    sudo ntpdate 0.fr.pool.ntp.org

Configurer la carte:

    sudo olimex-config

Mettre à jour la première ligne dans `/etc/hosts`

    127.0.0.1 localhost automateGazo

Remplir toute la carte SD:

    su & cd
    ./resize_sd.sh mmcblk0 2

Install Ajenti
--------------
Pas sur de l'intéret
http://ajenti.org/

    wget -O- https://raw.github.com/ajenti/ajenti/1.x/scripts/install-ubuntu.sh | sudo sh


Config perso
------------

    git clone https://github.com/oryxr/config-perso.git
    cd config-perso
    ./install.sh
    $ chsh  ->  /bin/zsh
    $ su
    # chsh  -> /bin/zsh

Installation ungit
------------------
https://github.com/FredrikNoren/ungit

    sudo apt-get install nodejs-legacy npm
    sudo -H npm install -g ungit

Rendre le nom persistant du convertisseur USB-RS485
---------------------------------------------------

Brancher le convertisseur, regarder dans quel `/dev/tty...` il est mis puis :

    udevadm info -a -n /dev/ttyUSB1

Créer un fichier `/etc/udev/rules.d/99-usb-serial.rules` avec :

    SUBSYSTEM=="tty", ATTRS{idVendor}=="0403", ATTRS{idProduct}=="6001", SYMLINK+="ttyRS485"

Link : http://hintshop.ludvig.co.nz/show/persistent-names-usb-serial-devices/

Compléter les éléments avec les équivalents trouvé précédement.

Installation python et dépendances
----------------------------------

    sudo apt-get install  build-essential libncurses5-dev libncursesw5-dev libreadline6-dev libdb-dev libgdbm-dev libsqlite3-dev libssl-dev libbz2-dev libexpat1-dev liblzma-dev zlib1g-dev libffi-dev gcc make python3-dev python3-pip
    sudo pip3 install crossbar pyA20 redis
    git clone https://github.com/bashwork/pymodbus.git -b python3
    cd pymodbus
    sudo python3 setup.py install

Installation dépendance javascript
----------------------------------

    sudo -H npm install -g bower
    cd /home/olimex/automateALTIE/frontend/js
    bower install

