Linux kiosk
===========
with kiosk-browser
==================

Compilation
-----------
    git clone https://github.com/pschultz/kiosk-browser.git
    cd kiosk-browser
    git checkout --track origin/feature/scrollbar
    sudo apt-get install libwebkit-dev
    make

Configuration de LXDE pour démarrer le Kiosk
--------------------------------------------
Editer : `/etc/xdg/lxsession/LXDE/autostart` ou `/home/olimex/.config/lxsession/LXDE/autostart`

    # Commenter les lignes du fichier et ajouter
    @xset s off
    @xset -dpms
    @xset s noblank
    @setxkbmap fr
    @/home/olimex/kiosk-browser/browser http://...

___
with iceweasel
==============
Pour avoir uniquement le navigateur web qui se lance au démarrage.

Installation IceWeasel
----------------------
    sudo apt-get install x11-xserver-utils iceweasel matchbox xautomation unclutter

Script de démarrage
-------------------

Créer le script: `/home/olimex/fullscreen.sh` et mettre:

    unclutter &
    matchbox-window-manager &
    iceweasel http://localhost:8080 --display=:0 &
    sleep 15s;
    xte -x :0 "key F11"

Le rendre exécutable:

    chmod +x /home/olimex/fullscreen.sh

Configuration de LXDE pour démarrer le Kiosk
--------------------------------------------
Editer : `/etc/xdg/lxsession/LXDE/autostart` ou `/home/olimex/.config/lxsession/LXDE/autostart`

    # Commenter les lignes du fichier et ajouter
    @xset s off
    @xset -dpms
    @xset s noblank
    @setxkbmap fr
    @/home/olimex/fullscreen.sh

Raffraichissement automatique de la page
----------------------------------------
Mettre dans la crontab : `crontab -e`

    0 */1 * * * xte -x :0 "key F5"