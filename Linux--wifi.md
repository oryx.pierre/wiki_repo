Linux et le wifi
================

Installation des drivers wifi non-libre
---------------------------------------
Editer `/etc/atp/source-list` et rajouter à la fin de la ligne `contrib non-free`

    apt-get update && apt-get install firmware-realtek firmware-ralink wireless-tools

Redémarrer

Configuration wifi
------------------

    iwconfig
    ip link set wlan0 up
    iwlist scan

Editer `/etc/network/interfaces`

    # my wifi device
    auto wlan0
    iface wlan0 inet dhcp
        wpa-ssid "[ESSID]"
        wpa-psk [PASSPHRASE] 

Sauver puis:

    ifup wlan0


Liens
-----
- https://wiki.debian.org/rtl819x
- https://wiki.debian.org/WiFi/HowToUse