
Git
===

https://grafikart.fr/tutoriels/checkout-revert-reset-586#autoplay

Ligne de commandes
------------------
- Initialisation dépot
    git init

- Status du dépot:
    git status

- Ajouter un fichier à suivre pour le prochain commit: "stage"
    git add

- Commit:
    git commit -m "Message"

- Log: obtention des infos
    git log --oneline

- Comparer les fichiers modifier:
    git diff --staged ou --cached

- Défaire un commit
    git revert

- Destager un fichier stage
    git reset -- fichier
    git reset HEAD fichier

- Tout destager sans perdre donné:
    git reset
    git reset num_commit: garde les modifs et supprime l'historique 

- Tout destager et perdre les données
    git reset --hard

- Remisage
    git stash save name stash
    git stash apply
    git stash drop
    git stash branch new_branch


- Remote
    git config --global branch.autosetuprebase always

- Clone avec pas tous l'historique
    git clone ... --depth 1




github
------

Quand on a "forké" un dépôt et que l'on souhaite le mettre à jour par rapport à l'original:
- Créer un nouveau remote: `git remote add upstream 'address original'`
- Récupérer toutes les modifications qui ont été faite un `git fetch upstream`
- Aller sur la branche que l'on souhaite metttre à jour: `git checkout 3.0`
- Mise à jour du dépôt: `git merge upstream/3.0`
- Pousser les modification `git push origin`

source: https://www.grafikart.fr/tutoriels/divers/git-workflow-478

gitolite
--------

ungit
-----
belle interface git web

source: https://www.grafikart.fr/formations/git/ungit
git

---

### reflog
Annuler un git reset --hard (move in 'ungit')
  
    git reflog

retourne:

    a28a6d6 HEAD@{9}: checkout: moving from syncRedisInModbus to master
    afd2550 HEAD@{8}: reset: moving to afd25500c4085ee8c2964731c954172ebaf28f98
    3241060 HEAD@{9}: commit: go to bower install javascript package

exécuter ensuite

    git reset --hard HEAD@{9}

### Annuler dernier commit

    git reset HEAD^

Pour intégrer les dernières modif au dernier commit

    git add .
    git commit --amend