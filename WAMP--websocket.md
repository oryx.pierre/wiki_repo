Autobahn JS
===========

Options
-------

    var options= {match: 'wildcard', acknowledge: true};
    $wamp.subscribe("gazo..state", function(args, kwargs, details){
      console.log(args, kwargs, details)
    }, options);

match: 'wildcard' or 'prefix'