Configuration des Altivar 31 ou 312
===================================

Modbus
------

### Réglage Altivar

- CTL :
  - LAC = L3
  - CHCF = SIM (consigne et commande non séparé)
  - FR1 = Mdb
- COM :
  - Add = 1 (adresse de l'esclave)
  - tbr = 19.2 (baudrate de 19200)
  - tFO = 8E1 (8 bit de données - parité paire - 1 bit de Stop)
  - **tt0 = 10** (délai de fonctionnement de l'altivar avant de basculer en défaut SLF (défaut Modbus). Nécessite une communication par modbus durant ce délai (lecture ou écriture de registre)
- drC : (réglage moteur)


- FLt :
  - OPL = Yes (nO pour les tests) (détection perte phase moteur)
  - SLL = FSt (détection défaut Modbus avec arrêt rapide) (nO pour les test, inhibe le `tt0`)


### Démarrage, arrêt moteur

Adresse des registres DRIVECOM :
- R
  - ETAD : 8603
  - RFRD : 8604
- R/W
  - CMDD : 8601
  - LFRD : 8602

1. R ETAD > 0x0240 (576)
2. W CMDD < 0x0006 (6)
3. R ETAD > 0x0221 (545)
4. W CMDD < 0x0007 (7)
5. R ETAD > 0x0223 (547)
6. W LFRD < 200 (vitesse moteur en rpm)

puis

7. W CMDD < 0x000F (15) sens direct
8. R ETAD + RFRD > 0x0627 + 200

ou 

9. W CMDD < 0x080F (2063) sens inverse
10. R ETAD + RFRD > 0x#### + 200


- Stop moteur : CMDD < 0x0000 (0)
- Reset défaut : CMDD < 0x0080 (128)