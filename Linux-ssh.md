Utilisation avancée de SSH
==========================
Création d'un Reverse SSH
-------------------------

Permet de passer outre les firewall : création d'un tunel ssh.

Avoir `AllowTcpForwarding yes` dans `/etc/ssh/sshd_config` sur le serveur

pour lancer le tunel à partir du serveur à accéder:

    ssh -nNT -R 1111:localhost:22 <utilisateurSpecialSshReverse>@oryxr.fr -p 22

pour s'y connecter : 

    ssh pierre@localhost -p 1111

Possibilité de réaliser un reverse pour un autre port par exemple `8080:localhost:80`

Pour cela ajouter dans `/etc/ssh/sshd_config`

    Match User <utilisateurSpecialSshReverse>
      GatewayPorts yes


#### Liens utiles
- [raspberry-pi-phoning-home-using-a-reverse-remote-ssh-tunnel](http://www.tunnelsup.com/raspberry-pi-phoning-home-using-a-reverse-remote-ssh-tunnel)
- [Reverse_ssh_:_Accéder_à_un_serveur_derrière_un_NAT_-_Firewall](http://wiki.kogite.fr/index.php/Reverse_ssh_:_Acc%C3%A9der_%C3%A0_un_serveur_derri%C3%A8re_un_NAT_-_Firewall)



Authorized Keys
---------------

    cd ~/.ssh
    ssh-keygen
    scp -P 22 id_rsa.pub <user>@<server>
    ssh -p 22 <user>@server
    cat id_rsa.pub >> ~/.ssh/authorized_keys

On ajoute les clés dans le fichier authorized_keys

Autoreconnexion du tunnel SSH
-----------------------------
[https://www.everythingcli.org/ssh-tunnelling-for-fun-and-profit-autossh/](https://www.everythingcli.org/ssh-tunnelling-for-fun-and-profit-autossh/)

    sudo apt-get install autossh

Vérifier que la connexion est bien effective et marche sans mdp (avec clé ssh)

    autossh -M 0 -o "ServerAliveInterval 30" -o "ServerAliveCountMax 3" -o "ExitOnForwardFailure yes" -nNT -R <portConnection>:localhost:22 -p <portServer> <sshreverseuser>@<server>

Création du service sous `systemd`.

    sudo vim /etc/systemd/system/autossh-ssh-tunnel.service

Et y mettre :

    [Unit]
    Description=AutoSSH tunnel service everything ssh on local port 11111
    After=network.target
    
    [Service]
    User=pi
    Environment="AUTOSSH_GATETIME=0"
    ExecStart=/usr/bin/autossh -M 0 -o "ServerAliveInterval 30" -o "ServerAliveCountMax 3" -o "ExitOnForwardFailure yes" -nNT -R <portConnection>:localhost:22 -p <portServer> <sshreverseuser>@<server>
    
    [Install]
    WantedBy=multi-user.target

Pour rechager les modules systemd `sudo systemctl daemon-reload`

Pour démarrer le service: `sudo systemctl start autossh-ssh-tunnel.service`

Pour arrêter le service: `sudo systemctl stop autossh-ssh-tunnel.service`

Pour voir l'état du service: `sudo systemctl status autossh-ssh-tunnel.service`

Pour activer le service au démarrage: `sudo systemctl enable autossh-ssh-tunnel.service`

Pour désactiver le service au démarrage: `sudo systemctl disable autossh-ssh-tunnel.service`

Si problème :

     sudo netstat -plant | grep 11111
     sudo kill <process>