Créer un HotSpot Wifi sous Linux
================================

- _hostapd_ : c’est lui qui va nous servir à créer un réseau wifi avec toutes les sécurités nécessaires sur l’interface wifi wlan1.
- _dnsmasq_ : il permettra à notre raspberry de jouer le rôle de serveur DHCP, pour attribuer des adresses IP automatiquement aux périphériques autorisés à se connecter au point d’accès
- _iptables_ : iptables sert à créer des rêgles de routage. C’est grâce à ces rêgles que nous pourrons rediriger la connexion d’une interface réseau vers une autre.


    sudo apt-get update
    sudo apt-get install hostapd iptables dnsmasq

hostapd
-------
connaitre le chipset de la carte

    lsusb | grep RTL8188CUS

Si `RTL8188CUS` alors installation du driver et compilation/installation du patch d'hostapd :

    cd /tmp
    sudo wget http://fichiers.touslesdrivers.com/39144/RTL8188C_8192C_USB_linux_v4.0.2_9000.20130911.zip
    sudo unzip RTL8188C_8192C_USB_linux_v4.0.2_9000.20130911.zip
    cd RTL8188C_8192C_USB_linux_v4.0.2_9000.20130911
    cd wpa_supplicant_hostapd
    sudo tar -xvf wpa_supplicant_hostapd-0.8_rtw_r7475.20130812.tar.gz
    cd wpa_supplicant_hostapd-0.8_rtw_r7475.20130812
    cd hostapd
    sudo make
    sudo make install
    sudo mv hostapd /usr/sbin/hostapd
    sudo chown root.root /usr/sbin/hostapd
    sudo chmod 755 /usr/sbin/hostapd

Sinon, configuration de `/etc/hostapd/hostapd.conf`:

    interface=wlan0
    driver=nl80211
    ssid=ALTIE_WIFI
    hw_mode=g
    channel=6
    macaddr_acl=0
    auth_algs=1
    ignore_broadcast_ssid=0
    wpa=2
    wpa_passphrase=36kva_ademe
    wpa_key_mgmt=WPA-PSK
    wpa_pairwise`=TKIP
    rsn_pairwise=CCMP

Edite `/etc/default/hostapd`

    DAEMON_CONF="/etc/hostapd/hostapd.conf"

dnsmasq
-------
Edite `/etc/dnsmasq.conf`

    interface=wlan0
    dhcp-range=192.168.200.100,192.168.200.200,255.255.255.0,12h

Nos clients wifi se connectant sur l’interface wlan1 se verront attribuer une IP entre 192.168.200.100 et 192.168.200.200, avec un masque de sous-réseau 255.255.255.0, et un bail de 12 heures

IPtables
--------
### Activation du port Forwarding ###
Edite `/etc/sysctl.conf` et mettre à la fin:

    net.ipv4.ip_forward=1

Pour l'activer temporairement :

    sudo sh -c "echo 1 > /proc/sys/net/ipv4/ip_forward"

Configuration d'IPtables

    sudo iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
    sudo iptables -A FORWARD -i eth0 -o wlan0 -m state --state RELATED,ESTABLISHED -j ACCEPT
    sudo iptables -A FORWARD -i wlan0 -o eth0 -j ACCEPT

**Pour éviter d'avoir à faire la config à chaque reboot**

    sudo sh -c "iptables-save > /etc/iptables.ipv4.nat"

Edite `/etc/network/interfaces` et ajouter à la fin:

    allow-hotplug wlan0
    iface wlan0 inet static
    address 192.168.200.1
    netmask 255.255.255.0
    network 192.168.200.0
    broadcast 192.168.200.255
    
    up iptables-restore < /etc/iptables.ipv4.nat

Redémarrer les services :

    sudo service hostapd restart
    sudo service dnsmasq restart

Pour lancer les services au démarrage :

    sudo update-rc.d hostapd enable
    sudo update-rc.d dnsmasq enable

Udev
----
modifier `/etc/udev/rules.d/70-persistent-net.rules` pour choisir wlan0

sources
-------
[http://hardware-libre.fr/2014/02/raspberry-pi-creer-un-point-dacces-wifi/](http://hardware-libre.fr/2014/02/raspberry-pi-creer-un-point-dacces-wifi/)

[http://elinux.org/RPI-Wireless-Hotspot](http://elinux.org/RPI-Wireless-Hotspot)